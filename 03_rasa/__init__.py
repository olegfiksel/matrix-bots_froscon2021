import random

from opsdroid.matchers import match_rasanlu, match_regex
from opsdroid.skill import Skill

class Rasa_PoC(Skill):
    def __init__(self, opsdroid, config):
      super(Rasa_PoC, self).__init__(opsdroid, config)
      # Load custom configuration part
      self.conf = self.opsdroid.config['skills']['conference_assistant']['config']

    @match_regex(r'hi|hello|hey|hallo', case_sensitive=False)
    async def hello(self, message):
        text = random.choice(
            ["Hallo {} 👋!", "Hi {} 👋!", "Wie geht's {} 👋?"]).format(message.user)
        text += " Ich bin ein Konferenz-Bot. Frage mich etwas."
        await message.respond(text)

    @match_rasanlu('question_programm')
    async def question_programm(self, message):
        await message.respond("Das Programm findest Du hier: {}".format(self.conf['programm_link']))

    @match_rasanlu('question_matrix_dev_room')
    async def question_matrix_dev_room(self, message):
        await message.respond("Matrix Dev-Room findet im Raum C120. Und es gibt auch einen Vortrag im Raum HS4.")

    @match_rasanlu('question_konferenz_start')
    async def question_konferenz_start(self, message):
        await message.respond(self.conf['konferenz_start'])

    @match_rasanlu('question_clients')
    async def question_clients(self, message):
        await message.respond("Ich nutze Neochat, Nheko und FluffyChat. Es gibt aber viele Clients. Schau hier: https://matrix.org/clients")

    @match_rasanlu('question_was_ist_matrix')
    async def question_was_ist_matrix(self, message):
        await message.respond("Matrix ist ein offenes Protokoll für föderierte, sichere Echtzeit-Kommunikation. Mehr Infos gibt es auf https://matrix.org")