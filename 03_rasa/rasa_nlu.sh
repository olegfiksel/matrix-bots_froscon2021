docker run --rm -ti -p 5005:5005 \
  --name rasa \
  rasa/rasa:2.6.2-full \
  run --enable-api --auth-token very-secure-rasa-auth-token -vv
