docker run --rm -ti \
  -v `pwd`/configuration.yaml:/root/.config/opsdroid/configuration.yaml:ro \
  -v `pwd`/__init__.py:/skills/conference_assistant/__init__.py \
  -v `pwd`/intents.yml:/skills/conference_assistant/intents.yml \
  --env-file ../secrets.env \
  --link rasa \
  opsdroid/opsdroid:v0.23.0
