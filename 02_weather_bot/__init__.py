import random

from opsdroid.matchers import match_regex
from opsdroid.skill import Skill

import aiohttp
import asyncio

import logging
_LOGGER = logging.getLogger(__name__)

# Source of inspiration: https://docs.opsdroid.dev/en/stable/examples/weather.html

class Wetter(Skill):
    def __init__(self, opsdroid, config):
      super(Wetter, self).__init__(opsdroid, config)
      # Load custom configuration part
      self.conf = self.opsdroid.config['skills']['wetter']['config']

    async def _get_weather(self, city):
        url = "{}?q={}&units={}&appid={}".format(
           self.conf['openweathermap_url'], city, self.conf['openweathermap_units'], self.conf['openweathermap_appid'])
        async with aiohttp.ClientSession() as session:
            response = await session.get(url)
        result = await response.json()
        _LOGGER.debug(_(result))
        return result

    @match_regex(r'weather|wetter', case_sensitive=False, matching_condition='search')
    async def hello(self, message):
        text = random.choice(["Ich schaue aus dem Fester...", "Ich schaue nach dem Wetter..."])
        await message.respond(text)
        await asyncio.sleep(self.conf['wait'])
        city = random.choice(self.conf['cities'])
        weather_data = await self._get_weather(city)
        # TODO: cut the temperature string after "." (20.05 => 20)
        answer = "In {} is es {} C".format(city,weather_data['main']['temp'])
        await message.respond(answer)
