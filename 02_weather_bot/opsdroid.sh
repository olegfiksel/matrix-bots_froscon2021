docker run --rm -ti -p 8080:8080 \
  -v `pwd`/configuration.yaml:/root/.config/opsdroid/configuration.yaml:ro \
  -v `pwd`/__init__.py:/skills/wetter/__init__.py \
  --env-file ../secrets.env \
  opsdroid/opsdroid:v0.23.0
