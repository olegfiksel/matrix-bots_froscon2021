docker run --rm -ti \
	-v `pwd`/configuration.yaml:/root/.config/opsdroid/configuration.yaml:ro \
	-v `pwd`/__init__de.py:/skills/hello/__init__.py \
	--env-file ../secrets.env \
	opsdroid/opsdroid:v0.23.0
