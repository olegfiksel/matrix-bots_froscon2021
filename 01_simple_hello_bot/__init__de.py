import random

from opsdroid.matchers import match_regex
from opsdroid.skill import Skill


class HelloByeSkill(Skill):

    @match_regex(r'hi|hello|hey|hallo', case_sensitive=False)
    async def hello(self, message):
        text = random.choice(
            ["Hallo {}!", "Hi {}!", "Wie geht's {}!"]).format(message.user)
        await message.respond(text)

    @match_regex(r'tsch[ueü]+s|bis sp[aeä]+ter|ich bin off|bis demn[aeä]+chst|bye', case_sensitive=False)
    async def goodbye(self, message):
        text = random.choice(
            ["Tschüs {}", "Bis später {}", "Auf wiedersehen {}", "Bis demnächst {}"]).format(message.user)
        await message.respond(text)
