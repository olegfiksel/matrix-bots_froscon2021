# Code for the "Matrix - Bots" talk @ FrOSCon 2021

* https://programm.froscon.de/2021/events/2658.html


# 🎥 Video

* [Youtube](https://www.youtube.com/watch?v=5sGfKUURBFs)
* [Media.ccc.de](https://media.ccc.de/v/froscon2021-2658-matrix_-_bots)
